package id.auliamr.binar.tugasdialog

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import id.auliamr.binar.tugasdialog.databinding.FragmentFirstBinding


class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showAlertDialog()
        showAlertDialogWithAction()
    }

    private fun showAlertDialog(){
        binding.btnDialog.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Kenalan, Yuk!")
            dialog.setMessage("Nama Aul. Asal Jakarta tapi sekarang lagi ngerantau ke Malang. Kira-kira puasa nanti balik gak ya?")
            dialog.setCancelable(true)
            dialog.show()
        }
    }

    private fun showAlertDialogWithAction(){
        binding.btnDialogAction.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Disini Judul")
            dialog.setMessage("Nama Aul. Asal Jakarta tapi sekarang lagi ngerantau ke Malang. Kira-kira puasa nanti balik gak ya?")
            dialog.setPositiveButton("Ya") { dialogInterface, angka ->
                createToast("Pulang aja bisa bukber bareng temen SMA, lho!").show()
            }
            dialog.setNegativeButton("Tidak") { dialogInterface, angka ->
                createToast("Gausah dah, ribet packing tau.").show()
            }
            dialog.setCancelable(false)
            dialog.show()

        }
    }

    private fun showAlertDialogCustom() {
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.custom_dialog, null, false)

        val title = customLayout.findViewById<TextView>(R.id.tv_title)
        val buttonOne = customLayout.findViewById<Button>(R.id.btn_one)

        title.text = "Ini sudah di ubah"
        buttonOne.text = "Dismiss"

        val builder = androidx.appcompat.app.AlertDialog.Builder(requireContext())

        builder.setView(customLayout)

        val dialog = builder.create()

        buttonOne.setOnClickListener {
            dialog.dismiss()
        }

        binding.btnDialogCustom.setOnClickListener {
            dialog.show()
        }
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
    }

    private fun createSnackBar(it: View, message: String): Snackbar {
        return Snackbar.make(it, message, Snackbar.LENGTH_SHORT)
    }

}